/**********************************************************
 ***************** TIPS ***********************************
 *********************************************************/

// 1st TIPS:
const array1 = [55, 21, 29, 99, 2];
const newArray = [...array1, 200, 666]; // on créé 1 copie du 1er array en utilisant "...", spread operator? à verifier
console.log(newArray);

// 2nd TIPS:
// REDUCE
/*2. Additionnez toutes les valeurs d'un tableau.
Mon instinct initial était d'utiliser une boucle, mais cela aurait été un gaspillage.*/
var numbers = [300, 5, 2];
var sum = numbers.reduce((x, y) => x + y);
console.log(sum); // affiche 307 (300+5+2) !!!

//3. Conditions de court-circuit.
//Nous avons le code suivant:
//if (hungry) {
//  goToFridge();
//}
// Nous pouvons le rendre encore plus court en utilisant la variable avec la fonction:
//hungry && goToFridge();

// 4 Trick: Supprimez les doublons d'un tableau!
//Cette astuce est assez simple. Supposons que j'ai un tableau contenant des nombres, des chaînes et des booléens.
//Et dans ce tableau, je veux m'assurer qu'il n'y a pas d'élément en double. Alors, comment faites-vous cela?

const array = [1, 2, 3, 2, 1, true, true, false, "Ratul", 1, 5];
const filtered__array = [...new Set(array)]; // Set empêche les doublons
console.log(filtered__array); // [ 1, 2, 3, true, false, 'Ratul', 5 ]

/* Trick-2: Transformez un nombre décimal en entier. */
//Celui-ci est un truc assez simple. Laisse moi te montrer.

const number = 23.6565;
console.log(number | 0);

/* Trick-3: Obtenir la dernière valeur d'un tableau! */
//Supposons que vous ayez un tableau de quelque chose. Maintenant, si vous voulez avoir le dernier élément du tableau, comment allez-vous faire cela?

const array2 = [
  1,
  2,
  3,
  4,
  5,
  "hey c'est moi la dernière valeur k tu chope avec .slice sur l'index -1!",
];
const last_item = array2.slice(-1);
console.log(last_item);
/* Explication .slice(index): */
/*Nous y voilà! Maintenant, si vous mettez -2 au lieu de -1, vous obtiendrez les deux dernières valeurs du tableau, 
puis si vous donnez -3 au lieu de -2, vous obtiendrez la valeur des trois derniers index et ainsi de suite.*/

////////////////////
/* Astuce-4: Obtenez une valeur d'index aléatoire à partir d'un tableau. */
//Supposons que nous organisions un programme de loterie. Nous avons un tableau qui
//contient les noms des participants. Maintenant, nous voulons qu'un seul utilisateur du tableau choisisse au hasard un gagnant.

const participants = ["Ratul", "George", "july", "Padrik", "G"];
const winner = participants[Math.floor(Math.random() * participants.length)];
console.log(winner);

/**********************************************************************************************
 ***************** destruction est une expression JavaScript ***********************************
 ************************************************************************************************/

/* La destruction est une expression JavaScript qui permet de décompresser les valeurs des tableaux, 
ou les propriétés des objets, dans des variables distinctes. Autrement dit, nous pouvons extraire des données de tableaux et d'objets et les affecter à des variables. ... L'affectation de déstructuration ES6 facilite l'extraction de ces données.

Commencer:
Permettez-moi de vous présenter la syntaxe de la déstructuration. Au début, nous apprendrons la déstructuration des tableaux

Déstructuration de la baie:
Lorsque vous utilisez des tableaux en Javascript. Comment accéder aux valeurs avant, en utilisant le numéro d'index droit? */

// const ara = ["Apple", "Mango", "Kiwi"];
// console.log(ara[0]);
// //Avant es6, c'était la seule façon de traiter les tableaux. J'espère que vous connaissez cela. Voyons maintenant quelle est la syntaxe moderne.

// const ara1 = ["Apple", "Mango", "Kiwi"];
// const [firstone, secondOne, thirdOne] = ara1;
// console.log(firstOne); // will return Apple which is the first item of our array

// const firstone = ara1[0],
//   secondOne = ara1[1],
//   thirdOne = ara1[2];

/*
N'est-ce pas si simple! .... Vous devez examiner certaines choses lorsque vous utilisez la déstructuration des tableaux. 
Quels sont, --- N'oubliez pas d'utiliser des crochets lorsque vous déstructurez le tableau en utilisant les variables.
Si vous n'utilisez pas les crochets ou si vous utilisez d'autres crochets, cela ne fonctionnera pas car nous travaillons avec des tableaux.

Déstructuration d'objets;

Nous allons maintenant apprendre la déstructuration des objets qui est assez similaire à la déstructuration des tableaux.
 Les différences sont auparavant dans la déstructuration des tableaux, nous utilisions des crochets et dans la déstructuration des objets, 
 nous utiliserons des accolades. Simple! Et autre chose avant de pouvoir donner un nom aux variables mais en déstructuration d'objet. 
 Vous devez utiliser le nom de propriété exact de l'objet dans le nom de la variable. Laissez-moi vous montrer un exemple rapide.*/

const bio = {
  name: "Hugues Gouttebroze",
  profession: "Concépteur Developpeur d'Application",
  passion: "JavaScript, CSS",
};

const { name, profession, passion } = bio;

console.log(
  "Hey, je m'appel " + name,
  "je bosse en tant que " + profession,
  "& je pràtique le " + passion,
  "durant mon temps libre"
);
// convert in template string
console.log(
  "Hey, je m'appel " + `${name}`,
  `je bosse en tant que ${profession}`,
  `& je pràtique le ${passion}`,
  "durant mon temps libre"
);

//Simple. Utilisez simplement des accolades au lieu du carré et utilisez le nom de propriété de l'objet comme nom de
//variable. Et tout ira bien. Et une chose est d'essayer de maintenir la séquence des propriétés de l'objet comme la première
//propriété est le nom et la seconde est l'âge. Donc, lorsque vous déstructurez l'objet, il devrait ressembler à ça,

//const { name, age } = objectName;

/* (...) Opérateur d'épandage */
/*
Nous allons maintenant en apprendre davantage sur l'opérateur de propagation. Alors qu'est-ce que c'est et comment ça marche? 
Supposons que vous ayez un tableau de certains noms et que vous deviez maintenant placer tous les éléments du tableau dans un nouveau 
tableau. Donc, dans ce cas, nous pouvons utiliser cet opérateur de propagation. Laissez-moi vous montrer un exemple. */

const names = ["Ratul", "George", "Jack"];

const newNames = [...names, "July", "Bosh"];
console.log(newNames);

//Dans ce code, je mets toutes les valeurs sous les noms ara dans ce tableau newName. Et dans le tableau newName,
//j'ai d'autres données supplémentaires qui ne sont pas disponibles dans notre tableau de noms. Voilà donc comment fonctionne
//l'opérateur de diffusion. Vous pouvez avoir toutes les données de n'importe quel tableau ou objet dans une nouvelle variable simplement en
//l'utilisant. Maintenant, comment l'utiliser dans les objets? Laisse moi te montrer.

const personData = {
  name: "George",
  age: 24,
  profession: "Android Developer",
};

const georgesData = {
  ...personData,
  data_of_birth: "bla bla bla",
};

console.log(georgesData);

/* Dans ce code, j'ai un objet nommé personData. Qui contient des informations sur une personne. Maintenant, 
je veux mettre dans un autre objet individuel qui sera nommé georgesData. Parce que ces données concernent George. 
Alors, comment faisons-nous cela? C'est facile, il suffit de créer un nouvel objet, puis d'utiliser l'opérateur de diffusion. 
Après cela, j'ai ajouté quelques données / informations supplémentaires dans l'objet georgesData. Quelle est sa date de naissance. 
//C'est donc une question de déstructuration d'objets.

C'est donc tout ce que je sais sur la déstructuration des tableaux et des objets. J'espère que vous avez apprécié cela. 
Si vous rencontrez un problème concernant ce message, veuillez me le faire savoir. Je vais prendre les mesures que je peux prendre. 
Et assurez-vous de me suivre pour recevoir tous les messages d'information comme ça.
*/

const frameworks = ["React", "Angular", "Vue", "Spring", "Laravel", "Symfony"];
const languages = ["JavaScript", "HTML", "CSS", "SASS", "Java", "PHP"];
const sgbd = ["MySQL", "PostGreSQL", "MariaDB", "SqlLite", "MongoDB"];
const hobbies = ["daily watch", "application's programming", "Arts"];
const environnement = ["Linux"];
const versioning = ["Git"];
const techSkills = [
  {
    ...frameworks,
    ...languages,
    ...sgbd,
    ...environnement,
    ...versioning,
  },
];

const humanSkills = [
  {
    personnalityTraits:
      "creativity, curiosity, open-mindedness, tolerance, knowledge sharing, benevolence, empathy",
    fonctionnality: "",
    ...hobbies,
  },
];

const bioDev = { techSkills, humanSkills, hobbies };

console.log(techSkills, humanSkills);

console.log(bioDev);

for (techSkill of techSkills) {
  console.log(techSkill);
}

/*
technical environment, functional programming, object-oriented design, 

, 

design patterns, object-oriented design


*/
